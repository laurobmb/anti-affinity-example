# Install end execute app

## Create new enviroment 
* python3 -m venv venv
* source venv/bin/activate

## Install requirements
* pip install --upgrade pip
* pip install -r app/requirements.txt

## Execute app
* python app/app.py

## Delete enviroment 
* deactivate 
* rm -rf venv