import subprocess,sys
import uvicorn
from fastapi import FastAPI, Request
from pydantic import BaseModel
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import FileResponse
from subprocess import Popen

app=FastAPI()

class infos(BaseModel):
    date: str
    path: str
    uptime: str
    hostname: str

def hostname():
    try:
        process = subprocess.run(['hostname'], check=True, stdout=subprocess.PIPE, universal_newlines=True )
        output = process.stdout
    except:
        process = subprocess.run(['cat','/etc/hostname'], check=True, stdout=subprocess.PIPE, universal_newlines=True )
        output = process.stdout
    return output

def date():
    process = subprocess.run(['date'], check=True, stdout=subprocess.PIPE, universal_newlines=True )
    output = process.stdout
    return output

def uptime():
    process = subprocess.run(['uptime'], check=True, stdout=subprocess.PIPE, universal_newlines=True )
    output = process.stdout
    return output

def path():
    process = subprocess.run(['pwd'], check=True, stdout=subprocess.PIPE, universal_newlines=True )
    output = process.stdout
    return output

@app.get("/", response_model=infos)
async def infos_web():
    return infos(
        date = date(),
        path = path(),
        uptime = uptime(),
        hostname = hostname()
    )
    
@app.get('/healthz')
async def healthz():
    return "up"

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8080)
