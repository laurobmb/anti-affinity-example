# Example Antiaffinity

## Create project
* oc new-project antiaffinity-example

## Deploy app
* oc new-app --name antiaffinity-example --labels app=fastapi https://gitlab.com/laurobmb/anti-affinity-example.git#main --context-dir app --strategy=source

## Exposer service
* oc expose service antiaffinity-example 

## Create healthcheck
* oc set probe deployment/antiaffinity-example --readiness --initial-delay-seconds=10 --timeout-seconds=30 --get-url=http://:8080/healthz
* oc set probe deployment/antiaffinity-example --liveness --initial-delay-seconds=10 --timeout-seconds=30 --get-url=http://:8080/healthz

## Scale app
* oc scale deployment antiaffinity-example --replicas 5

## Apply pod antiaffinity 
* oc patch deployment/antiaffinity-example --patch-file k8s/antiaffinity.yml

## Check access
* host=`oc get route antiaffinity-example -o jsonpath='{.spec.host}{"\n"}'`
* curl --insecure $host | jq '{hostname}'
